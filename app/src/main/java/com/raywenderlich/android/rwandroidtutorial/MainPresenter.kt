package com.raywenderlich.android.rwandroidtutorial

/**
 * PRESENTER holds instance of Repository through Dependency Injector (MODEL) and View interface from MainContract (VIEW)
 */
class MainPresenter(view: MainContract.View, dependencyInjector: DependencyInjector) :
    MainContract.Presenter {

    private val weatherRepository: WeatherRepository = dependencyInjector.weatherRepository()

    //View can be activity or fragment
    private var view: MainContract.View? = view

    override fun onViewCreated() {
        loadWeather()
    }

    override fun onLoadWeatherClicked() {
        loadWeather()
    }

    //Remove activity or fragment from presenter
    override fun onDestroy() {
        this.view = null
    }

    private fun loadWeather() {
        val weather = weatherRepository.loadWeather()
        val weatherState = weatherStateForWeather(weather)

        //On MODEL update, modify VIEW
        view?.displayWeatherState(weatherState)
    }

    private fun weatherStateForWeather(weather: Weather): WeatherState {
        if (weather.rain!!.amount!! > 0) {
            return WeatherState.RAIN
        }
        return WeatherState.SUN
    }
}