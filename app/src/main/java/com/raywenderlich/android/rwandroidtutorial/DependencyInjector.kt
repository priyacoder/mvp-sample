package com.raywenderlich.android.rwandroidtutorial

/**
 * Replace in production with libraries (like Dagger2, Koin etc.)
 */
interface DependencyInjector {

    fun weatherRepository(): WeatherRepository
}