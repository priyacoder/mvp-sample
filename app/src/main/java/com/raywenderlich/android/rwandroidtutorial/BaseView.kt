package com.raywenderlich.android.rwandroidtutorial

/**
 * Interface to be implemented by all views in app
 * As view interacts with a presenter, a generic type <T> is given
 */
interface BaseView<T> {

    fun setPresenter(presenter: T)
}