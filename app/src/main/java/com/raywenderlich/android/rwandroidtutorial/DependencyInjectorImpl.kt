package com.raywenderlich.android.rwandroidtutorial

/**
 * Returns the weather repository instance
 * It is better to implement the interface this way so that
 * this can be replaced later with another implementation if required.
 */
class DependencyInjectorImpl : DependencyInjector {
    override fun weatherRepository(): WeatherRepository = WeatherRepositoryImpl()
}