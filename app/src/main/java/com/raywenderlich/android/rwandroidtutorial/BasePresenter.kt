package com.raywenderlich.android.rwandroidtutorial

/**
 * A generic interface any presenter in the app should implement.
 */
interface BasePresenter {
    // A facade for Android lifecycle callback
    fun onDestroy()
}