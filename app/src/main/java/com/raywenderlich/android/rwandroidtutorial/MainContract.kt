package com.raywenderlich.android.rwandroidtutorial

/**
 * Interface for <Main> Activity which inherits from base view and presenter interfaces.
 */
interface MainContract {

    interface Presenter : BasePresenter {
        fun onViewCreated()
        fun onLoadWeatherClicked()
    }

    interface View : BaseView<Presenter> {
        fun displayWeatherState(weatherState: WeatherState)
    }
}